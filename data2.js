var database = [
 {
  foto:"img/mieayam.jpeg", 
  nama: 'Mie ayam',
  harga: "Rp16.000"
}, 
{
  foto:"img/baksourat.jpg", 
  nama: 'Bakso Urat', 
  harga: "Rp20.000"
}, 
{
  foto:"img/aqua.jfif",
  nama:'Aqua',
  harga:"Rp2000"
},
{
  foto:"img/bakso.jpg", 
  nama: 'Bakso biasa', 
  harga: "Rp15.000"
}, 
{
  foto:"img/makanan6.jpg", 
  nama: 'Nasi goreng', 
  harga: "Rp15.000"
},  
{
  foto:"img/ayamgoyeng.jpg", 
  nama: 'Nasi Ayam', 
  harga: "Rp10.000"
}, 
{
  foto:"img/mie.jpg", 
  nama: 'Mie Sehat', 
  harga: "Rp10.000"
},  
{
  foto:"img/seblak.jpg", 
  nama: 'Seblak Mercon',
  harga: "Rp8.000"
},  
{
  foto:"img/teh.jpg", 
  nama: 'Es_Teh', 
  harga: "Rp4.500"
},   
{
  foto:"img/escampur.jpg", 
  nama: 'ABCD', 
  harga: "Rp10.000"
 },
 ];


// database.map((data, key) => {
//   let menu = `<div>
//         <img src= "${data.gambar}" />
//         <h2>${data.nama}</h2>
//         <p>${data.desc}</p>
//         <h2>${data.harga}</h2>
//     </div>`;

//   const dataMenu = document.querySelector(".menu");
//   dataMenu.innerHTML += menu;
// });

database  
.filter((data) => data.harga <= 25000)
  .sort((a, b) => (a.harga > b.harga ? 1 : b.harga > a.harga ? -1 : 0))
  .map((data, key) => {
    let menu = `<div class="box-makanan ">
      <img src="${data.foto}" style="height:200px; width:300px;">
          <h5 class="card-title">${data.nama}</h5>
          <p class="card-text">${data.harga}</p>
          <a href="#" class="btn">Beli</a>`;

    const dataMenu = document.querySelector("body");
    dataMenu.innerHTML += menu;
  });
