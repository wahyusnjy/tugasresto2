const dataMakanan =[
{
	foto:"img/mieayam.jpeg", 
	nama: 'Mie ayam',
	harga: "Rp16.000"
}, 
{
	foto:"img/baksourat.jpg", 
	nama: 'Bakso Urat', 
	harga: "Rp20.000"
}, 
{
	foto:"img/aqua.jfif",
	nama:'Aqua',
	harga:"Rp2000"
},
{
	foto:"img/bakso.jpg", 
	nama: 'Bakso biasa', 
	harga: "Rp15.000"
}, 
{
	foto:"img/makanan6.jpg", 
	nama: 'Nasi goreng', 
	harga: "Rp15.000"
},  
{
	foto:"img/ayamgoyeng.jpg", 
	nama: 'Nasi Ayam', 
	harga: "Rp10.000"
}, 
{
	foto:"img/mie.jpg", 
	nama: 'Mie Sehat', 
	harga: "Rp10.000"
},  
{
	foto:"img/seblak.jpg", 
	nama: 'Seblak Mercon',
	harga: "Rp8.000"
},  
{
	foto:"img/teh.jpg", 
	nama: 'Es_Teh', 
	harga: "Rp4.500"
},   
{
 	foto:"img/escampur.jpg", 
 	nama: 'ABCD', 
 	harga: "Rp10.000"
 },
 ];

const callbackMap = (item, index)=>{
  const elmnt = document.querySelector('.daftar-makanan');
  
  elmnt.innerHTML += `
    <div class="box-makanan ">
      <img src="${item.foto}" style="height:200px; width:300px;">
          <h5 class="card-title">${item.nama}</h5>
          <p class="card-text">${item.harga}</p>
          <a href="form.html" class="btn">Beli</a>
        </div>
   `
 
 }

dataMakanan.map(callbackMap);


const buttonElemnt = document.querySelector('.button-search');

buttonElemnt.addEventListener('click', ()=>{
  const hasilPencarian = dataMakanan.filter((item, index)=>{
                const inputElemnt    =document.querySelector('.input-keyword');
                const buttonElemnt   = document.querySelector('.button-search');
                const namaItem       = item.nama.toLowerCase();
                const keyword      = inputElemnt.value.toLowerCase();

                return namaItem.includes(keyword);
        })

  document.querySelector('.daftar-makanan').innerHTML ='';

  hasilPencarian.map(callbackMap);
});


